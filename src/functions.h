#include <sys/types.h> 
#include <dirent.h>

#define MAX_CMDLINE_LEN 4096
#define reset_path(path) strcpy(*path, "/proc/")

typedef char bool;

#ifndef false
#define false 0
#endif

#ifndef true
#define true !false
#endif

unsigned int get_pid_dirents_size (const struct dirent** pid_dirents);
unsigned int get_pid_dirnames (char*** pid_dirnames);
struct dirent** get_pid_dirents ();

int filter_dir(const struct dirent* entry);
int pid_dir_compare (const struct dirent **first_entry, const struct dirent **second_entry);
bool is_number (const char* string);

char* correct_comm (char* comm);
void remove_newline(char* string);

time_t get_proc_time ();
time_t get_cpu_time ();
time_t get_uptime ();
void fill_boot_time();

char* procinfo_to_string (const struct procinfo p_info);

pid_t find_process (const struct procinfo* p_info_arr, const unsigned int size, const char* proc_name);

void enter_decimal (int* number, char* msg);
void get_string (char* string, size_t size);

bool should_continue (char* msg);
