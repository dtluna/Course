#include "functions.h" // <sys/types.h> <dirent.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include "procinfo.h"
#include "signals.h"


const char* const SIGNAL_NAMES[] = {"SIGHUP", "SIGINT", "SIGQUIT", "SIGILL", "SIGTRAP",
	"SIGABRT", "SIGBUS", "SIGFPE", "SIGKILL", "SIGUSR1",
	"SIGSEGV", "SIGUSR2", "SIGPIPE", "SIGALRM", "SIGTERM",
	"SIGSTKFLT", "SIGCHLD", "SIGCONT", "SIGSTOP", "SIGTSTP",
	"SIGTTIN", "SIGTTOU", "SIGURG", "SIGXCPU", "SIGXFSZ",
	"SIGVTALRM", "SIGPROF", "SIGWINCH", "SIGIO", "SIGPWR",
	"SIGSYS", "SIGRTMIN", "SIGRTMIN+1", "SIGRTMIN+2", "SIGRTMIN+3",
	"SIGRTMIN+4", "SIGRTMIN+5", "SIGRTMIN+6", "SIGRTMIN+7", "SIGRTMIN+8",
	"SIGRTMIN+9", "SIGRTMIN+10", "SIGRTMIN+11", "SIGRTMIN+12", "SIGRTMIN+13",
	"SIGRTMIN+14", "SIGRTMIN+15", "SIGRTMAX-14", "SIGRTMAX-13", "SIGRTMAX-12",
	"SIGRTMAX-11", "SIGRTMAX-10", "SIGRTMAX-9", "SIGRTMAX-8", "SIGRTMAX-7",
	"SIGRTMAX-6", "SIGRTMAX-5", "SIGRTMAX-4", "SIGRTMAX-3", "SIGRTMAX-2",
	"SIGRTMAX-1","SIGRTMAX"};

void send_signals (const struct procinfo* p_info_arr, const unsigned int size)
{
	char proc_name[256];
	char signal_name[11];
	int signum = 0;
	pid_t pid = 0;
	do
	{
		printf("Enter PID or name of the process to send signal to: ");
		get_string(proc_name, 256);
		if (is_number(proc_name))
			pid = atoi(proc_name);
		else
		{	
			pid = find_process(p_info_arr, size, proc_name);
			if (-1 == pid)
				continue;
		}

		printf("Enter the signal to send (default is 9, SIGKILL): ");
		get_string(signal_name, 11);
		if ( 0 == strcmp("", signal_name) )
			signum = 9;
		else
			if (is_number(signal_name))
			{
				signum = atoi(signal_name);
				if (signum >= 64 || signum < 1)
				{
					fprintf(stderr, "Error: no such signal!\n");
					continue;
				}
			}
			else
			{
				for (int i = 0; i < 62; ++i)
					if (0 == strcmp(SIGNAL_NAMES[i], signal_name))
						signum = i+1;

				if (0 == signum)
				{
					fprintf(stderr, "Error: no such signal!\n");
					continue;
				}
			}

		int ret_val = kill(pid, signum);
		if (-1 == ret_val)
		{
			perror("kill");
			continue;
		}
		continue;
	}while(should_continue("Do you want to send one more signal"));
}
