void check_error (long long return_value, long long error_value, char* message);

#define NULL_error(e, msg) check_error(e, NULL, msg);
#define fopen_err(e) NULL_error(e, "fopen");
#define malloc_err(e) NULL_error(e,"malloc");
#define fgets_err(e) NULL_error(e,"fgets");

#define EOF_error(e, msg) check_error(e, EOF, msg);
#define fclose_err(e) EOF_error(e, "fclose");
#define fscanf_err(e) EOF_error(e, "fscanf");

#define minus_one_error(e, msg) check_error(e, -1, msg);
#define chdir_err(e) minus_one_error(e,"chdir");
#define scandir_err(e) minus_one_error(e,"scandir");
#define stat_err(e) minus_one_error(e, "stat");
