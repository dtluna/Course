#include "system_info.h"
#include "functions.h" 
#include "errors.h"

void update_sys_info ()
{
	sys_info.uptime = get_uptime();
	sys_info.cpu_time = get_cpu_time();
}
