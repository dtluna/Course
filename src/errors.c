#include "errors.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

void check_error (long long return_value, long long error_value, char* message)
{
	if (error_value == return_value)
	{
		perror(message);
		exit(EXIT_FAILURE);
	}
}
