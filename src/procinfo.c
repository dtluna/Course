#include "procinfo.h" //sys/types.h included
#include "functions.h" // <sys/types.h> <dirent.h>
#include "errors.h"
#include "system_info.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

struct procinfo* get_procinfo_array(const char** pid_dirnames, const unsigned int dirnames_num)
{
	struct procinfo* p_info_arr = alloc_procinfo_array (dirnames_num);
	char* path = alloc_str(12);//6 "/proc/", ещё 5 на pid и 1 на '\0'
	update_sys_info();
	reset_path(&path);
	for (unsigned int i = 0; i < dirnames_num; i++)
	{
		strcat(path, pid_dirnames[i]);
		chdir_err(chdir(path));
		fill_procinfo( &(p_info_arr[i]) );
		chdir_err(chdir(".."));
		reset_path(&path);
	}
	free(path);
	return p_info_arr;
}

void fill_procinfo (struct procinfo* p_info)
{
	FILE* stat_file = fopen("./stat", "r");
	fopen_err(stat_file);
	clock_t starttime_clk = 0;
	fscanf(stat_file, 
		"%d %s %c %d %*d"
		"%*d %*d %*u %*u %*lu"
		"%*lu %*lu %*lu %*lu %*lu"
		"%*ld %*ld %ld %ld %ld"
		"%*ld %llu %lu %ld",
		&(p_info->pid), p_info->comm, &(p_info->state), &(p_info->ppid),
		&(p_info->priority), &(p_info->nice), &(p_info->num_threads),
		&starttime_clk, &(p_info->vsize), &(p_info->rss));

	p_info->starttime = starttime_clk/sysconf(_SC_CLK_TCK);
	p_info->comm = correct_comm (p_info->comm);
	fclose_err(fclose(stat_file));

	p_info->uptime = sys_info.uptime - p_info->starttime;
	p_info->starttime = sys_info.boot_time + p_info->starttime;

	return;
}

void fill_cpu_usage (struct procinfo** p_info_arr, unsigned int size, const char** pid_dirnames)
{
	update_sys_info();
	time_t* first_proc_time = alloc_time_t_array(size);
	time_t* second_proc_time = alloc_time_t_array(size);
	time_t first_cpu_time = sys_info.cpu_time;
	time_t second_cpu_time = 0;

	char* path = alloc_str(12);//6 "/proc/", ещё 5 на pid и 1 на '\0'
	reset_path(&path);
	for (unsigned int i = 0; i < size; i++)
	{
		strcat(path, pid_dirnames[i]);
		chdir_err(chdir(path));
		first_proc_time[i] = get_proc_time();
		chdir_err(chdir(".."));
		reset_path(&path);
	}

	sleep(1);

	update_sys_info();
	second_cpu_time = sys_info.cpu_time;
	for (unsigned int i = 0; i < size; i++)
	{
		strcat(path, pid_dirnames[i]);
		chdir_err(chdir(path));
		second_proc_time[i] = get_proc_time();

		(*p_info_arr)[i].cpu_usage = (100*(second_proc_time[i]-first_proc_time[i]))/(second_cpu_time - first_cpu_time);

		chdir_err(chdir(".."));
		reset_path(&path);
	}

	free(first_proc_time);
	free(second_proc_time);
	free(path);
	return;
}

