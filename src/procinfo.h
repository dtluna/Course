#include <sys/types.h>

struct procinfo
{
	pid_t pid; //1 %d
	pid_t ppid; //4 %d

	char* comm;//2 %s
	char state;//3 %c

	long priority;//18 %ld
	long nice;//19 %ld

	size_t num_threads;//20 %ld

	time_t starttime;//22 %llu
	time_t uptime;
	int cpu_usage; //высчитываем
	size_t rss;//24 %ld
	size_t vsize;//23 %lu
};

struct procinfo* get_procinfo_array(const char** pid_dirnames, const unsigned int dirnames_num);
void fill_procinfo (struct procinfo* p_info);
void fill_cpu_usage (struct procinfo** p_info_arr, unsigned int size, const char** pid_dirnames);
