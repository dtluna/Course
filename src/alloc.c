#include "procinfo.h"
#include "alloc.h"
#include "errors.h"
#include "functions.h"
#include <stdlib.h>

char** alloc_str_array (size_t nmemb)
{
	char** str_array = (char**)calloc(nmemb, sizeof(char*));
	malloc_err(str_array);
	return str_array;
}

char* alloc_str (size_t length)
{
	char* string = (char*)calloc(length, sizeof(char));
	malloc_err(string);
	return string;
}

time_t* alloc_time_t_array (size_t nmemb)
{
	time_t* array = (time_t*) calloc(nmemb, sizeof(time_t));
	malloc_err(array);
	return array;
}

struct procinfo* alloc_procinfo_array (size_t nmemb)
{
	struct procinfo* procinfo_array = (struct procinfo*) calloc(nmemb, sizeof(struct procinfo));
	malloc_err(procinfo_array);
	
	for (size_t i = 0; i < nmemb; ++i)
		procinfo_array[i].comm = alloc_str(MAX_CMDLINE_LEN);
	
	return procinfo_array;
}

