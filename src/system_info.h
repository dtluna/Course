#include <sys/types.h>

struct system_info
{
	time_t uptime; //полное время работы системы
	time_t cpu_time; //полное время работы процессора
	time_t boot_time;
} sys_info;

void update_sys_info ();
