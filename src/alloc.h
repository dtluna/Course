#include <sys/types.h>

struct procinfo* alloc_procinfo_array (size_t nmemb);
char** alloc_str_array (size_t nmemb);
char* alloc_str (size_t length);
time_t* alloc_time_t_array (size_t nmemb);
