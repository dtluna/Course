#include "procinfo.h"
#include "change_prio.h"
#include "functions.h"
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


void change_prio (const struct procinfo* p_info_arr, const unsigned int size)
{
	char proc_name[256];
	pid_t pid = 0;
	int prio = 0;

	do
	{
		printf("Enter PID or name of the process to change priority: ");
		get_string(proc_name, 256);
		if (is_number(proc_name))
			pid = atoi(proc_name);
		else
		{	
			pid = find_process(p_info_arr, size, proc_name);
			if (-1 == pid)
				continue;
		}

		enter_decimal(&prio, "Enter the new priority of process: ");

		int ret_val = setpriority(PRIO_PROCESS, pid, prio);
		if (-1 == ret_val)
		{
			perror("setpriority");
			continue;
		}
		
	}while(should_continue("Do you want to change priority one more time"));
}
