#include "procinfo.h"
#include "functions.h" // <sys/types.h> <dirent.h>
#include "errors.h"
#include "signals.h"
#include "change_prio.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void help(int argc, const char** argv);
void killing (int argc, const char** argv, struct procinfo* p_info_arr, unsigned int size);
void changing_prio (int argc, const char** argv, struct procinfo* p_info_arr, unsigned int size);

int main(int argc, const char** argv)
{
	help(argc, argv);

	fill_boot_time();
	chdir_err(chdir("/proc/"));
	char** pid_dirnames = NULL;
	unsigned int dirnames_num = get_pid_dirnames(&pid_dirnames);

	struct procinfo* p_info_arr = get_procinfo_array (pid_dirnames, dirnames_num);
	
	puts("  PID S  PPID %CPU  PRIO  NICE   THR UPTIME     VSIZE     RSS COMM");
	for (unsigned int i = 0; i < dirnames_num; ++i)
		puts(procinfo_to_string (p_info_arr[i]));

	killing(argc, argv, p_info_arr, dirnames_num);
	changing_prio(argc, argv, p_info_arr, dirnames_num);
	
	free(pid_dirnames);
	free(p_info_arr);
	return 0;
}

void help(int argc, const char** argv)
{
	for (int i = 1; i < argc; ++i)
	{
		if ((0 == strcmp(argv[i], "-h")) || (0 == strcmp(argv[i], "--help")) )
		{
			printf("Usage:%s [options...]\n", argv[0]);
			puts("Options:");
			puts("-h, --help — print this message and exit;");
			puts("-k, --kill — allow signal sending;");
			puts("-c, --chprio — allow priority changes.");
			exit(0);
		}
	}
}

void killing (int argc, const char** argv, struct procinfo* p_info_arr, unsigned int size)
{
	for (int i = 1; i < argc; ++i)
	{
		if ((0 == strcmp(argv[i], "-k")) || (0 == strcmp(argv[i], "--kill")))
		{
			send_signals(p_info_arr, size);
			return;
		}
	}
}

void changing_prio (int argc, const char** argv, struct procinfo* p_info_arr, unsigned int size)
{
	for (int i = 1; i < argc; ++i)
	{
		if ((0 == strcmp(argv[i], "-c")) || (0 == strcmp(argv[i], "--chprio")))
		{
			change_prio(p_info_arr, size);
			return;
		}
	}
}
