CC = gcc
SRC_DIR = src/
CFLAGS = -c -std=gnu11 -g
LDFLAGS = 
HEADERS = $(SRC_DIR)errors.h  $(SRC_DIR)procinfo.h  $(SRC_DIR)functions.h $(SRC_DIR)system_info.h $(SRC_DIR)signals.h $(SRC_DIR)change_prio.h $(SRC_DIR)alloc.h
SOURCES =  $(SRC_DIR)errors.c  $(SRC_DIR)main.c  $(SRC_DIR)procinfo.c  $(SRC_DIR)functions.c $(SRC_DIR)system_info.c $(SRC_DIR)signals.c $(SRC_DIR)change_prio.c $(SRC_DIR)alloc.c
OBJECTS = $(SOURCES:.c=.o)
EXECUTABLE = main

all: $(HEADERS) $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(OBJECTS) $(LDFLAGS) -o $@

.c.o: $(SOURCES)
	$(CC) $(CFLAGS) $< -o $@

clean: $(HEADERS) $(SOURCES)
	rm $(OBJECTS) $(EXECUTABLE)

remake: $(HEADERS) $(SOURCES)
	rm $(OBJECTS) $(EXECUTABLE)
	$(CC) $(CFLAGS) $(SOURCES)
	$(CC) $(OBJECTS) $(LDFLAGS) -o $(EXECUTABLE)	
